# Entwicklung eines Editors für Annahmekalküle unter besonderer Berücksichtigung der Usability

## Installation

### Node

Angular ist nicht mit der aktuellen Node-Version kompatibel. Alle Versionen lassen sich mit [nvm](https://github.com/nvm-sh/nvm) installieren – auch parallel zu einander. Die kompatiblen Versionen sind [hier](https://www.redcricket.net/en/news-en/compatibility-list-for-angular-angular-cli-and-node-js/) aufgeführt.

Ich habe den Editor mit Angular 12.2 entwickelt. Die neueste kompatible Node-Version ist 14.15.

Nach der Installation von nvm:

```
nvm install 14.15
```

### Paketanforderungen des Editors

Danach muß einmal im Ordner `app` im Repo Folgendes ausgeführt werden, um benötigte Pakete zu installieren.

```
npm install

```

## Ausführen

Im selben Ordner wird ein lokaler Server gestartet.

```
ng serve
```

In der Ausgabe steht der Port, über den die Anwendung zu erreichen ist. Der Editor nutzt eine CSS-Regel, die bisher nur in Firefox implementiert ist. In anderen modernen Browsern funktioniert er aber auch, nur das Layout ist weniger dynamisch.
