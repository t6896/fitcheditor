import { Component } from '@angular/core';
import { KatexOptions } from 'ng-katex';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Fincher App';

  taskTex: string = '\\neg (A \\wedge \\neg B) \\to  (A\\to B)';
  latexOptions: KatexOptions = {
    displayMode: true,
  };
}
