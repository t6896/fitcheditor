import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { KatexModule } from 'ng-katex';

import { AppComponent } from './app.component';
import { FitchDiagramComponent } from './fitch-diagram/fitch-diagram.component';
import { FitchLineComponent } from './fitch-line/fitch-line.component';
import { FitchInputComponent } from './fitch-input/fitch-input.component';
import { FitchAnnotationComponent } from './fitch-annotation/fitch-annotation.component';
import { FitchKeyboardComponent } from './fitch-keyboard/fitch-keyboard.component';

@NgModule({
  declarations: [
    AppComponent,
    FitchDiagramComponent,
    FitchLineComponent,
    FitchInputComponent,
    FitchAnnotationComponent,
    FitchKeyboardComponent
  ],
  imports: [
    BrowserModule,
    KatexModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
