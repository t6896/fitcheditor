import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FitchAnnotationComponent } from './fitch-annotation.component';

describe('FitchAnnotationComponent', () => {
  let component: FitchAnnotationComponent;
  let fixture: ComponentFixture<FitchAnnotationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FitchAnnotationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FitchAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
