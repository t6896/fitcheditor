import { Component, ElementRef, OnInit, Output, Renderer2, EventEmitter, HostBinding } from '@angular/core';
import { FitchInputComponent } from '../fitch-input/fitch-input.component';

@Component({
  selector: 'app-fitch-annotation',
  templateUrl: './fitch-annotation.component.html',
  styleUrls: [
    '../fitch-input/fitch-input.component.css', // css vererben
    './fitch-annotation.component.css'
  ]
})
export class FitchAnnotationComponent extends FitchInputComponent {

  constructor(_element:ElementRef, renderer: Renderer2) {
    super(_element, renderer);
  }

  ngAfterViewInit(){
    super.ngAfterViewInit();

    this.preventLosingFocusOnSuggestion();
  }

  /**
   * Wenn auf einen Vorschlag geklickt wird, verliert das Input den Fokus und verschwindet.
   * Diese Methode verhindert das. 
   * Einmal beim Initialisieren ausführen.
   */
  private preventLosingFocusOnSuggestion() {
    this._element.nativeElement.querySelector(".arrowBox")?.addEventListener("mousedown", (event:Event) => {
      event.stopPropagation()
      event.stopImmediatePropagation()
      event.preventDefault();
    });
  }

  /** 
   * Parent-Funktion wird erweitert um das Anzeigen der Vorschläge.
   * 
   * @see {../fitch-input.component.ts}
   */
  activateInput () {
    super.activateInput();
    this.showSuggestions();
  }

  /**
   * Parent-Funktion wird erweitert um das Ausblenden der Vorschläge.
   * 
   * @see {../fitch-input.component.ts}
   */
  public disableInput() {
    super.disableInput();
    this.hideSuggestions();
  }

  /**
   * Blendet Vorschläge aus.
   */
  private hideSuggestions() {
    this._element.nativeElement.querySelector(".arrowBox")?.classList.add("hidden"); 
  }

  /**
   * Blendet Vorschläge ein.
   */
  private showSuggestions() {
    setTimeout(() => {
      const arrayBox = this._element.nativeElement.querySelector(".arrowBox");
    
      arrayBox.classList.remove("hidden");
      const inputWidth = this._element.nativeElement.querySelector("input").offsetWidth;
      arrayBox.style.left = parseInt(inputWidth) + 15 + "px"; 
    }, 1);
  }

  /**
   * Setzt Inhalt des Inputs auf gegebenen Wert.
   * 
   * @param {string} suggestion
   * @see {./fitch-annotation.component.html}
   */
  public acceptSuggestion(suggestion: string) {    
    this.inputElement!.nativeElement.value = suggestion;    
    this.onChange();
    this.hideSuggestions(); 
  }

}
