import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FitchDiagramComponent } from './fitch-diagram.component';

describe('FitchDiagramComponent', () => {
  let component: FitchDiagramComponent;
  let fixture: ComponentFixture<FitchDiagramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FitchDiagramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FitchDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
