import { AfterViewInit, Component, ComponentFactory, ComponentFactoryResolver, ElementRef, OnInit, QueryList, ViewChildren, ViewContainerRef } from '@angular/core';
import { FitchAnnotationComponent } from '../fitch-annotation/fitch-annotation.component';
import { FitchInputComponent } from '../fitch-input/fitch-input.component';
import { FitchLineComponent } from '../fitch-line/fitch-line.component';

// Alias
type FLC = FitchLineComponent;
type FAC = FitchAnnotationComponent;
@Component({
  selector: 'app-fitch-diagram',
  templateUrl: './fitch-diagram.component.html',
  styleUrls: ['./fitch-diagram.component.css']
})
export class FitchDiagramComponent implements OnInit, AfterViewInit {

  private linesAndAnnotations:Map<FitchLineComponent, FitchAnnotationComponent> = new Map();
  private linesInOrder:Array<FitchLineComponent> = new Array();

  private fitchInputFactory: ComponentFactory<FitchInputComponent>;
  private fitchLineFactory: ComponentFactory<FitchLineComponent>;
  private fitchAnnotationFactory: ComponentFactory<FitchAnnotationComponent>;

  private counter:number = 2; // refactor

  constructor(private _element:ElementRef, private resolver: ComponentFactoryResolver, private _vcr: ViewContainerRef) {
    this.fitchLineFactory = resolver.resolveComponentFactory(FitchLineComponent);
    this.fitchAnnotationFactory = resolver.resolveComponentFactory(FitchAnnotationComponent);
    this.fitchInputFactory = resolver.resolveComponentFactory(FitchInputComponent);
  }

  ngAfterViewInit() {
    
    /* Angular ruft ngAfterViewInit auf, wenn das DOM initalisiert wird.
       Die Methode scheint aber in der Eventloop vor dem Aufbau des DOMs zu sein.       
    */
    window.requestAnimationFrame(() => {
      window.requestAnimationFrame(() => {
        this.addNewRow();
        this.repairView();
      });
    });

    this._element.nativeElement.querySelector("button.check").addEventListener("click", () => {
      console.log(this.toString())
    });
  }

  ngOnInit(): void {
  }

  /**
   * Setzt das HTML-Attribut tabindex, um mit der Tabtaste zwischen Inputs zu wechseln.
   */
  private setTabIndex() {
    let index:number = 0;
    this.linesInOrder.forEach((line:FLC) => {
      const annotation = this.linesAndAnnotations.get(line)!;
      index += 1;

      line._element.nativeElement.querySelector("input").tabIndex = index;
      line._element.nativeElement.querySelector("ng-katex").tabIndex = index;

      index += 1;

      annotation._element.nativeElement.querySelector("input").tabIndex = index;
      annotation._element.nativeElement.querySelector("ng-katex").tabIndex = index;
    });
  }

  /**
   * Fügt gegebener Annotation einen Listener (Enter -> neue Zeile) hinzu.
   * 
   * @param {FitchAnnotationComponent} annotation
   */
  private addEventListenerToAnnotation(annotation:FitchAnnotationComponent) {
    FitchDiagramComponent.getDomOfComponent(annotation).onkeyup = (event: KeyboardEvent) => {
      event.stopPropagation();
      event.preventDefault();
      if (event.code === "NumpadEnter" || event.code === "Enter") {
        const newRow = this.addNewRowAfter(annotation);
        newRow.line.activateInput();
      }

      return false;
    }
  }  

  /**
   * Liest Zeilen aus DOM ein und fixt Scopes, Buttons und Zeilennummern
   */
  private repairView () {
    this.removeEmptyScopes();
    this.updateLineNumbers();
    this.fixScopeRowSpan();
    this.updateLineControlElements()
    this.setTabIndex();
  }

  /**
   * Eine Zeile im Grid eines Scopes muß auch eine Zeile im gesamten Grid einnehmen,
   * damit nichts versetzt dargestellt wird.
   */
  private fixScopeRowSpan () {
    const scopes = this._element.nativeElement.querySelectorAll(".scope");
    scopes.forEach((scope: HTMLElement) => {
      const linesInScope = scope.querySelectorAll("app-fitch-line");
      scope.style.gridRowEnd = "span " + linesInScope.length;
    });
  }

  /**
   * Löscht alle Zeilennummern und generiert neue.
   */
  private updateLineNumbers() {
    this.removeLineNumbers();
    this.setLineNumbers();
  }

  /**
   * Löscht alle Zeilennummern
   */
  removeLineNumbers() {
    const lineNumbers = this._element.nativeElement.querySelectorAll(".lineNumber");
    lineNumbers.forEach((item: HTMLElement) => {
      item.remove();
    });
  }

  /**
   * Erstellt soviele Zeilennummern, wie <app-fitch-line>s im Diagram sind.
   */
  setLineNumbers(){
    this.linesInOrder.forEach((line:FLC, index:number) => {
      this._element.nativeElement.appendChild(createLineNumber(index+1));
    });

    function createLineNumber(i:number) {
      const lineNumberDom = document.createElement("div");
      lineNumberDom.classList.add("lineNumber");
      lineNumberDom.innerText = `${i.toString()}`;
      return lineNumberDom;
    }      
  }

  /**
   * Löscht alle Buttons (rechts neben Diagramm) und generiert neue.
   */
  public updateLineControlElements() {
    this.removeLineControlElements();
    this.setLineControlElements();
  }

  /**
   * Löscht alle Buttons (rechts neben Diagramm).
   */
  private removeLineControlElements() {
    const controlElements = this._element.nativeElement.querySelectorAll(".lineControls");
    controlElements.forEach((item:HTMLElement) => {
      item.remove();
    }); 
  }

  /**
   * Erstellt Buttons (rechts neben Diagramm) und fügt sie ins DOM ein.
   */
  private setLineControlElements() {
    // this wird in der Funktion createLineControlElements benötigt, aber von derselben überdeckt
    const self = this;

    this.linesInOrder.forEach((line:FLC) => {
      const controlsDom = createControlElements(line);
      this._element.nativeElement.appendChild(controlsDom);
    });

    function createControlElements(line:FLC):HTMLElement {
      // wrapper
      const controlsDom:HTMLElement = document.createElement("div");
      controlsDom.classList.add("lineControls");

      // Button add line
      const addLineBtn = document.createElement("i");
      addLineBtn.classList.add("addLine");
      addLineBtn.innerText = "↵";
      addLineBtn.title = "Add new line";
      addLineBtn.addEventListener("mousedown", event => {
        if (event.button === 0) {
          const newRow = self.addNewRowAfter(line);
          newRow.line.activateInput();
        }
      });
      controlsDom.appendChild(addLineBtn);

      // Button move in scope
      const newScopeBtn = document.createElement("i");
      newScopeBtn.classList.add("newScope");
      newScopeBtn.innerText = "▭";
      newScopeBtn.title = "Put in new scope";      
      newScopeBtn.addEventListener("mousedown", event => {
        if (event.button === 0)
          self.moveLineInNewScope(line);
      });
      controlsDom.appendChild(newScopeBtn);

      // Button move out of scope
      const upScopeBtn = document.createElement("i");
      upScopeBtn.classList.add("upScope");
      if (!self.isLineAtPeriphery(line)) {
        upScopeBtn.classList.add("disabled");
      } else {
        upScopeBtn.addEventListener("mousedown", event => {
          if (event.button === 0)
            self.moveLineOutOfScope(line);
        });
      }
      upScopeBtn.innerText = "◂";
      upScopeBtn.title = "Move out of scope";
      controlsDom.appendChild(upScopeBtn);

      // Button remove row
      const removeLineBtn = document.createElement("i");
      removeLineBtn.classList.add("remove");
      if (self.linesInOrder.length <= 1) {
        removeLineBtn.classList.add("disabled");
      } else {
        removeLineBtn.addEventListener("mousedown", event => {
          if (event.button === 0)
            self.removeRow(line);
        });      
      }
      removeLineBtn.innerText = "✖";
      removeLineBtn.title = "Remove line";
      controlsDom.appendChild(removeLineBtn);

      return controlsDom;
    }
  }

  /**
   * Löscht gegebene Zeile mit Annotation aus Map und DOM
   * 
   * @param {FitchLineComponent} line
   */
  public removeRow(line:FitchLineComponent) {
    const lineDom = FitchDiagramComponent.getDomOfComponent(line);
    const annotationDom = FitchDiagramComponent.getDomOfComponent(this.linesAndAnnotations.get(line)!);

    lineDom.style.opacity = "0";
    annotationDom.style.opacity = "0";

    this.linesInOrder.splice(this.linesInOrder.indexOf(line), 1);
    this.linesAndAnnotations.delete(line);

    //setTimeout(()=>{
      // Annotation aus DOM löschen
      annotationDom.remove();
      // Zeile löschen
      lineDom.remove();
      this.repairView();
    //}, 0);

  }  

  /**
   * Erstellt neuen Scope um gegebene Zeile (mit Annotation)
   * 
   * @param {FitchLineComponent} line
   */
  public moveLineInNewScope(line:FitchLineComponent) {
    let lineDom = FitchDiagramComponent.getDomOfComponent(line);
    let annotationDom = this.linesAndAnnotations.get(line)!._element.nativeElement;

    const scope = document.createElement("div");
    scope.classList.add("scope");

    const variable = this._vcr.createComponent(this.fitchInputFactory).instance;
    const variableDom = FitchDiagramComponent.getDomOfComponent(variable);
    variable.addCSSClass("variable");
    variableDom.addEventListener("click", () => {
      variable.activateInput();
    });

    lineDom.parentElement?.insertBefore(scope, lineDom);
    
    scope.appendChild(variableDom);
    scope.appendChild(lineDom);
    scope.appendChild(annotationDom);

    scope.insertBefore(variableDom, lineDom);

    this.repairView();
  }

  /**
   * Schiebt letze Zeile (mit Annotation) vor vorigen Scope oder letzte darunter.
   * 
   * @param {FitchLineComponent} line 
   */
   public moveLineOutOfScope(line: FitchLineComponent) {
    const lineDom = FitchDiagramComponent.getDomOfComponent(line);
    const parentDom = lineDom.parentElement as HTMLElement;
    const annotationDom = FitchDiagramComponent.getDomOfComponent(this.linesAndAnnotations.get(line)!);
    
    if (!annotationDom || !parentDom) {
      console.error(`Error in diagram structure: ${lineDom} does not have parent or annotation`);
    } else {
      if (this.isLineFirstInScope(line)) {
        parentDom.parentElement?.insertBefore(lineDom, parentDom);
        parentDom.parentElement?.insertBefore(annotationDom, parentDom);
      } else if (this.isLineLastInScope(line)) {
        FitchDiagramComponent.insertInDomAfter(lineDom, parentDom);
        FitchDiagramComponent.insertInDomAfter(annotationDom, lineDom);
      } else {
        console.error(`Line ${lineDom} can not be moved out of scope.`);
      }
    }

    this.repairView();
  }

  /**
   * Prüft, ob die gegebene Zeile am Anfang oder am Ende eines Scopes ist.
   * Ist die Zeile in keinem Scope, wird false zurückgegeben.
   * 
   * @param line {FitchLineComponent}
   * @returns {boolean}
   */
  public isLineAtPeriphery (line:FitchLineComponent):boolean {
    return this.isLineFirstInScope(line) || this.isLineLastInScope(line);
  }

  /**
   * Prüft, ob gegebene Zeile am Ende eines Scopes ist.
   * 
   * @param {FitchLineComponent} line
   * @returns {boolean}
   */
  private isLineLastInScope(line:FitchLineComponent):boolean {
    let lineDom = FitchDiagramComponent.getDomOfComponent(line);

    const annotationDom = FitchDiagramComponent.getDomOfComponent(this.linesAndAnnotations.get(line)!);
    const parentDom = lineDom.parentElement;

    if (!annotationDom || !parentDom) {
      console.error(`Error in diagram structure: ${lineDom} does not have parent or annotation`);
    } else {
      if (parentDom.classList.contains("scope")) {
        return parentDom.lastElementChild === annotationDom;
      }
    }

    return false;
  }  

  /**
   * Prüft, ob gegebene Zeile am Anfang eines Scopes ist.
   * 
   * @param {FitchLineComponent} line
   * @returns {boolean}
   */
  private isLineFirstInScope (line:FitchLineComponent) {
    let lineDom = FitchDiagramComponent.getDomOfComponent(line);
    const parentDom = lineDom.parentElement;

    if (!parentDom) {
      console.error(`Error in diagram structure: ${lineDom} does not have parent or annotation`);
    } else {
      if (parentDom.classList.contains("scope")) {
        const linesInScope = parentDom.querySelectorAll("app-fitch-line");
        return linesInScope[0] === lineDom;
      }      
    }

    return false;
  }

  /**
   * Löscht alle leeren .scope-Elemente aus dem DOM.
   */
  private removeEmptyScopes() {
    const scopes = this._element.nativeElement.querySelectorAll(".scope");
    scopes.forEach((scope:Element) => {
      const elementsInScope = scope.querySelectorAll("app-fitch-line");
      if (elementsInScope.length === 0) {
        scope.remove();
      }
    });
  }

  /**
   * Erstellt neue Zeile.
   * 
   * @returns {FitchLineComponent}
   */
  private createNewLine(): FitchLineComponent {
    return this._vcr.createComponent(this.fitchLineFactory).instance;
  }

  /**
   * Erstellt neue Annotation.
   * 
   * @returns {FitchAnnotationComponent}
   */
  private createNewAnnotation():FitchAnnotationComponent {
    return this._vcr.createComponent(this.fitchAnnotationFactory).instance;
  }

  /**
   * Fügt gegebene Zeile und Annotation
   * nach oder vor einem gegebenem Element oder am Ende des Diagramms
   * dem DOM hinzu.
   * 
   * @param {FitchLineComponent} line 
   * @param {FitchAnnotationComponent} annotation 
   * @param {FitchLineComponent | FitchAnnotationComponent} [referenceComponent] 
   * @param {boolean} [mountBefore] 
   */
  private addRowToDom(line:FLC, annotation:FAC, referenceComponent?:FLC|FAC, mountBefore?:boolean) {
    const lineDom = FitchDiagramComponent.getDomOfComponent(line);
    const annotationDom = FitchDiagramComponent.getDomOfComponent(annotation);

    if (!referenceComponent) {
      this._element.nativeElement.appendChild(lineDom);
      this._element.nativeElement.appendChild(annotationDom);
    } else {
      if (!mountBefore) {
        let prevAnnotation = referenceComponent;
        if (referenceComponent instanceof FitchLineComponent)        
          prevAnnotation = this.linesAndAnnotations.get(referenceComponent)!;
        
        FitchDiagramComponent.insertInDomAfter(lineDom, prevAnnotation);
        FitchDiagramComponent.insertInDomAfter(annotationDom, lineDom);
      } else {
        console.error("addLineAndAnnotationToDom mountBefore not implemented.")
      }

    }
  }

  /**
   * Erstellt neue Zeile und Annotation und fügt sie ans Ende des Diagramms an.
   * 
   * @returns { {FLC, FAC} }
   */
  public addNewRow():{line:FLC, annotation:FAC}  {
    const newLine = this.createNewLine();
    const newAnnotation = this.createNewAnnotation();

    this.linesInOrder.push(newLine);
    this.linesAndAnnotations.set(newLine, newAnnotation);

    this.addEventListenerToAnnotation(newAnnotation);

    this.addRowToDom(newLine, newAnnotation);

    this.repairView();

    return {line: newLine, annotation: newAnnotation};
  }

  /**
   * Erstellt neue Zeile mit Annotation, registriert sie und fügt sie nach
   * gegebener FLC|FAC dem DOM hinzu.
   * 
   * @param {FitchLineComponent | FitchAnnotationComponent} referenceComponent
   * @returns { {FLC, FAC} }
   */
  public addNewRowAfter(referenceComponent:FLC|FAC):{line:FLC, annotation:FAC} {
    const newLine = this.createNewLine();
    const newAnnotation = this.createNewAnnotation();
    let refLine:FLC;

    if (referenceComponent instanceof FitchAnnotationComponent) {
      refLine = this.getLineByAnnotation(referenceComponent)!;
    } else {
      refLine = referenceComponent;
    }
    
    this.linesInOrder.splice(this.linesInOrder.indexOf(refLine)+1, 0, newLine);

    this.linesAndAnnotations.set(newLine, newAnnotation);

    this.addEventListenerToAnnotation(newAnnotation);

    this.addRowToDom(newLine, newAnnotation, refLine);

    this.repairView();

    return {line: newLine, annotation: newAnnotation};
  }

  /**
   * Findet zugehörige Line einer Annotation.
   * 
   * @param {FitchAnnotationComponent} annotation
   * @returns {FitchLineComponent} 
   */
  private getLineByAnnotation(annotation:FitchAnnotationComponent):FLC|undefined {
    let line:FLC|undefined;
    this.linesAndAnnotations.forEach((a:FAC, l:FLC) => {
      if (a === annotation) {
        line = l;
        return false;
      } else {
        return true;
      }
    });
    return line;
  }

  /**
   * Gibt das HTMLElement einer Component zurück.
   * Wenn ein HTMLElement übergeben wird, wird es zurückgegeben.
   * 
   * @param {FitchLineComponent | FitchAnnotationComponent | FitchInputComponent | HTMLElement} object
   * @returns {HTMLElement}
   */
  static getDomOfComponent (object: FitchLineComponent | FitchAnnotationComponent | FitchInputComponent |HTMLElement): HTMLElement {
    if (object instanceof FitchLineComponent || object instanceof FitchAnnotationComponent || object instanceof FitchInputComponent) {
      return object._element.nativeElement;
    } else {
      return object;
    }
  }

  /**
   * Fügt newElement nach referenceElement in DOM ein.
   * 
   * @param {Element} newElement
   * @param {Element} referenceElement
   */
  static insertInDomAfter(toAdd:HTMLElement|FLC|FAC, ref:HTMLElement|FLC|FAC) {
    const toAddDom = this.getDomOfComponent(toAdd);
    const refDom = this.getDomOfComponent(ref);

    refDom.parentElement?.insertBefore(toAddDom, refDom.nextSibling);
  }

  /**
   * Rekursive Funktion -> Array mit Modell allen Inhalts 
   * {list, domElement} = scope
   * { {}, {} } = Eintrag besteht aus line und annotation
   * line und annotation = {content, domElement}
   * 
   * @param {Element} [domElement] 
   * @returns {[]}
   */
   private readDiagram (domElement:Element = this._element.nativeElement):any[] {
    const model: any[] = [];

    let query = ":scope > app-fitch-line, :scope > div.scope";
    if (domElement.matches(".scope")) {
      query = ":scope > app-fitch-line, :scope > div.scope";
    }

    const linesAndScopes = domElement.querySelectorAll(query); // :scope Funktionier nur in aktuellen Browsern

    linesAndScopes.forEach(item => {

      if (item.classList.contains("scope")) {
        model.push({list: this.readDiagram(item), domElement: item});
      } else {
        const lineContent = item.querySelector("input")?.value;
        if (lineContent === undefined) {
          console.error("app-fitch-line hat kein Input\n" + item);
        }
        const line = {content: lineContent, domElement: item};

        const annotationDom = item.nextSibling as HTMLElement;
        const annotationContent = annotationDom.querySelector("input")?.value;
        if (annotationContent === undefined) {
          console.error("app-fitch-annotation hat kein Input\n" + annotationDom);
        }
        const annotation = {content: annotationContent, domElement: annotationDom};

        model.push({line, annotation});
      }

    })

    return model;
  }

  private listToString(list:any[]):string {
    let s = "";

    s += "\\ll\n";

    list.forEach(item => {
      if (item.line) {
        s += this.counter + " " + replaceTex(item.line.content) + " \\mid " + replaceTex(item.annotation.content) + "\n";
        this.counter += 1;
      } else if (item.list) {        
        s += this.listToString(item.list);        
      }
    });

    s += "\\gg\n";

    return s;

    function replaceTex(s:string):string {
      s += " ";
      s = encodeURI(s);
      // \land zu \wedge
      s.replace(new RegExp('land ', 'g'), ' wedge ');
      // \lor zu \vee
      s.replace(new RegExp('lor ', 'g'), ' vee ');
      // \rightarrow zu ->
      s.replace(new RegExp('rightarrow', 'g'), ' -> ');
      
      return s;
    }
  }

  private listToGrammar = (function () {
    let counter = 0;

    return function (reset:boolean) {
      if (reset)
        counter = 0;
      
      
    };
  })();

  public toString():string {
    this.counter = 1;
    return this.listToString(this.readDiagram());
  }

}