import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FitchInputComponent } from './fitch-input.component';

describe('FitchInputComponent', () => {
  let component: FitchInputComponent;
  let fixture: ComponentFixture<FitchInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FitchInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FitchInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
