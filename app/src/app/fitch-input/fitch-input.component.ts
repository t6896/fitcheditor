import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'app-fitch-input',
  templateUrl: './fitch-input.component.html',
  styleUrls: ['./fitch-input.component.css']
})
export class FitchInputComponent implements AfterViewInit{

  tex: string = "";
  active: boolean = false;

  @ViewChild("input") inputElement?: ElementRef;

  katexDom?: HTMLElement | null;

  constructor(public _element:ElementRef, public renderer: Renderer2) {}

  ngAfterViewInit(){
    this.katexDom = this._element.nativeElement.querySelector("ng-katex")
    this.setActivationListener();
    this._element.nativeElement.classList.add("empty");
  }

  public addCSSClass(className:string) {
    this._element.nativeElement.classList.add(className);
  }

  /**
   * Setzt EventListener für die Aktivierung beim Fokussieren vom Input und Katex
   */
  private setActivationListener() {
    this.inputElement?.nativeElement.addEventListener("focus", () => {this.activateInput()});
    this.katexDom?.addEventListener("focus", () => {this.activateInput()});
  }

  /**
   * Aktualisiert tex bei Veränderungen im Input.
   * 
   * @see {./fitch-input.component.html}
   */
  public onChange() {
    this.tex = this.inputElement?.nativeElement.value;

    if (this.tex.length <= 0) {
      this._element.nativeElement.classList.add("empty");
    } else {
      this._element.nativeElement.classList.remove("empty");
    }
  } 

  /**
   * Deaktiviert Input und löscht die Listener aus this.activateInput()
   */
  private onBlur = () => {
    this.disableInput();
    // Listener für Deaktivierung wieder entfernen, da bereits deaktiviert
    this.inputElement?.nativeElement.removeEventListener("blur", this.onBlur);
    this.katexDom?.removeEventListener("blur", this.onBlur);
  };

  /**
   * Versteckt Katex und blendet Input ein.
   * Setzt Listener für Deaktivierung.
   */
  public activateInput() {
    // toggelt hidden wert, siehe html
    this.active = true;
    this.inputElement?.nativeElement.focus();

    // warten, bis input nicht mehr hidden ist
    setTimeout(()=>{
      // Listener für Deaktivierung

      this.inputElement?.nativeElement.addEventListener("blur", this.onBlur);
      this.katexDom?.addEventListener("blur", this.onBlur);     

      this.inputElement?.nativeElement.focus();
    }, 10);
  }

  /**
   * @see {./fitch-input.component.html}
   */
  public disableInput() {
    this.active = false;
  }

}
