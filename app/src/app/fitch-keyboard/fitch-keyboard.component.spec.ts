import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FitchKeyboardComponent } from './fitch-keyboard.component';

describe('FitchKeyboardComponent', () => {
  let component: FitchKeyboardComponent;
  let fixture: ComponentFixture<FitchKeyboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FitchKeyboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FitchKeyboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
