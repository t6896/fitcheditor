import { AfterViewInit, Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-fitch-keyboard',
  templateUrl: './fitch-keyboard.component.html',
  styleUrls: ['./fitch-keyboard.component.css']
})
export class FitchKeyboardComponent implements AfterViewInit {

  keypressed:boolean;

  constructor(public _element: ElementRef) {
    this.keypressed = false;
  }

  ngAfterViewInit() {
    this._element.nativeElement.addEventListener("mousedown", (event:Event) => {
      event.preventDefault();
    });

    this.setDragListeners();
    this.setClickListeners();

    this.setKeyListeners();

  }

  /**
   * Blendet alle Layouts aus.
   */
  private hideAllKeyboardLayouts():void {
    this._element.nativeElement.querySelectorAll(".keys").forEach((layout:HTMLElement) => {
      layout.style.display = "none";
    });
  }

  public showLogics() {    
    this._element.nativeElement.querySelector(".keys.logics").style.display = "grid";
    this._element.nativeElement.querySelector(".keys.lowerCaseGreek").style.display = "none";
    this._element.nativeElement.querySelector(".keys.upperCaseGreek").style.display = "none";
  }

  public showUpperCaseGreek() {
    this._element.nativeElement.querySelector(".keys.logics").style.display = "none";
    this._element.nativeElement.querySelector(".keys.lowerCaseGreek").style.display = "none";
    this._element.nativeElement.querySelector(".keys.upperCaseGreek").style.display = "grid"; 
  }

  public showLowerCaseGreek() {
    this._element.nativeElement.querySelector(".keys.logics").style.display = "none";
    this._element.nativeElement.querySelector(".keys.upperCaseGreek").style.display = "none";
    this._element.nativeElement.querySelector(".keys.lowerCaseGreek").style.display = "grid";
  }  

  public onButtonClick(button:HTMLElement):void {
    if (button.dataset.tex) {
      const activeInput = document.activeElement as HTMLInputElement;
      if (!activeInput)
        return;
      const textBeforeSelection = activeInput.value?.substring(0, activeInput.selectionStart!);
      const textAfterSelection = activeInput.value?.substring(activeInput.selectionEnd!);
      if (textBeforeSelection && textAfterSelection) {
        activeInput.value = textBeforeSelection + " " + button.dataset.tex + " " + textAfterSelection;
        const newCursorPosition = textBeforeSelection.length + button.dataset.tex.length;
      } else {
        activeInput.value += " " + button.dataset.tex + " "
      }
      // fire change event
      let event = document.createEvent("HTMLEvents");
      event.initEvent("change", true, false);
      activeInput.dispatchEvent(event);
    }
  }


  setClickListeners() {
    const buttons = this._element.nativeElement.querySelectorAll(".keys button");
    buttons.forEach((button: HTMLElement) => {
      if (!button.classList.contains("disabled")) {
        button.addEventListener("mousedown", (event: Event) => {
          event.preventDefault();          
          button.classList.add("active");
        });
        button.addEventListener("mouseup", (event: Event) => {
          button.classList.remove("active");
          this.onButtonClick(button);
        });          
      } else {
        button.addEventListener("mousedown", (event:Event) => {
          event.preventDefault();
          event.stopImmediatePropagation();
          event.stopPropagation();
        })
      }
    });
  }

  setDragListeners() {

    // https://www.w3schools.com/howto/howto_js_draggable.asp
    const self = this;
    dragElement(this._element.nativeElement);
    function dragElement(elmnt: HTMLElement) {
      var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

      self._element.nativeElement.querySelector(".headline")!.onmousedown = dragMouseDown;


      function dragMouseDown(e: MouseEvent) {
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        self._element.nativeElement.querySelector(".headline").style.cursor = "grabbing";
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
      }

      function elementDrag(e: MouseEvent) {
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = Math.min((elmnt.offsetLeft - pos1), document.body.clientWidth - elmnt.clientWidth) + "px";
      }

      function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
        self._element.nativeElement.querySelector(".headline").style.cursor = "grab";
      }
    }
  }

  private setKeyListeners() {
    document.addEventListener("keypress", (event:KeyboardEvent) => {
      const activeKeys = Array.from(this._element.nativeElement.querySelectorAll(".keys")).find((keysDom:any) => {
        return keysDom.clientHeight > 0;
      }) as HTMLElement;
      let button:Element|null;
      switch (event.code) {
        case "Numpad0":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(16)");          
          break;

        case "Numpad1":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(12)");          
          break;
      
        case "Numpad2":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(13)");          
          break;

        case "Numpad3":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(14)");          
          break;

        case "Numpad4":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(9)");          
          break;

        case "Numpad5":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(10)");          
          break;

        case "Numpad6":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(11)");          
          break;

        case "Numpad7":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(5)");          
          break;

        case "Numpad8":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(6)");          
          break;

        case "Numpad9":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(7)");          
          break;

        case "NumpadMultiply":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(3)");
          if (activeKeys.classList.contains("logics"))
            this.showUpperCaseGreek();
          else if (activeKeys.classList.contains("upperCaseGreek"))
            this.showLogics();
          else
            this.showLogics();   
          break;

        case "NumpadDecimal":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(17)");     
          break;

        case "NumpadAdd":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(8)");     
          break;

        case "NumpadSubtract":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(4)");     
          break;

        case "NumpadEnter":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(15)");     
          break;

        case "NumpadDivide":
          event.preventDefault();
          button = activeKeys.querySelector(":nth-child(2)");
          if (activeKeys.classList.contains("logics"))
            this.showLowerCaseGreek();
          else if (activeKeys.classList.contains("upperCaseGreek"))
            this.showLowerCaseGreek();
          else
            this.showUpperCaseGreek();  
          break;            

        default:
          button = null;
          //console.log(event.code)
          break;
      }

      if (button) {
        let evt = document.createEvent("HTMLEvents");
        evt.initEvent("mousedown", true, false);
        button?.dispatchEvent(evt);
        setTimeout(() => {
          evt = document.createEvent("HTMLEvents");
          event.initEvent("mouseup", true, false);
          button?.dispatchEvent(event);
        }, 100);        
      }      
    });
  }

}
