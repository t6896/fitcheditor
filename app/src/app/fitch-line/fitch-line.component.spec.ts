import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FitchLineComponent } from './fitch-line.component';

describe('FitchLineComponent', () => {
  let component: FitchLineComponent;
  let fixture: ComponentFixture<FitchLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FitchLineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FitchLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
