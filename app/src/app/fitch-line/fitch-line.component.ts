import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { KatexOptions } from 'ng-katex';
import { FitchInputComponent } from '../fitch-input/fitch-input.component';

@Component({
  selector: 'app-fitch-line',
  templateUrl: './fitch-line.component.html',
  styleUrls: [
    '../fitch-input/fitch-input.component.css',
    './fitch-line.component.css'
  ]
})
export class FitchLineComponent extends FitchInputComponent {

  constructor(_element:ElementRef, renderer: Renderer2) {
    super(_element, renderer);
  }

}
